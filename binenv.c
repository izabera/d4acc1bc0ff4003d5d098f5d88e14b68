#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main() {

  // slurp stdin
  char *file;
  size_t size;
  FILE *tmp = open_memstream(&file, &size);
  char buf[BUFSIZ];
  ssize_t s;
  while ((s = fread(buf, 1, BUFSIZ, stdin))) fwrite(buf, 1, s, tmp);
  fputc(0, tmp); // trailing 0 in all the strings in execve
  fclose(tmp);


  // count how many \0 in it
  size_t cnt = 1; // trailing NULL ptr in execve
  for (size_t i = 0; i < size; i++)
    if (!file[i]) cnt++;


  // build a new env pointer
  char **env = malloc(cnt * sizeof(char *)), **envp = env;
  *envp++ = file;

  for (size_t i = 0; i < size-1; i++) // don't include trailing \0
    if (!file[i]) *envp++ = file+i+1;

  *envp = 0;


  char *argv[] = { "/bin/cat", "/proc/self/environ", 0 };
  execve(argv[0], argv, env);
}
